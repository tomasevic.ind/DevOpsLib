from fastapi import FastAPI

app = FastAPI()

@app.get("/jenkins/parameters")
def get_jenkins_parameters():
    return {
        "branch": "main",
        "environment": "production",
        "ForceRebuild" : "${parameters['Force Rebuild']}",
        "ApprovedReorg" : "${parameters['Approved Reorg']}",
        "DeployAzure" : "${parameters['Deploy To Azure']}",
        #/* config parms */
        "gxInstallationId" : 'GX18_Thrwty',
        "msbuildId" : 'MsBuild17',
        "serverURL" : 'https://genexus.northeurope.cloudapp.azure.com/genexusserver18',
        "kbName" : 'Thrwty',
        "kbVersion" : 'Thrwty',
        "environment" : 'DevelopmentEnv',
        "envPath" : 'ENV',
        "localKbPath" : 'Thrwty',
        "localKbVersion" : 'Thrwty',
        "sqlServerInstance" : 'vmJenkinsWin\\SQLEXPRESS',
        "sqlserverCredentials" : 'none',
        "credentialsId" : 'GxServer18',
        "LocalSettingsFile" : 'BuildOnly.settings',
        "deployToCloud" : 'false',
        "metadataFilePath" : 'Metadata\\pipelinemetadata',
        "CreateDbInKbFolder" : 'false',
        "KBLogs" : 'Logs',
        "Verbosity" : '/verbosity:minimal',
        }
# uvicorn main:app --host (ip de la maquina) --port 8000