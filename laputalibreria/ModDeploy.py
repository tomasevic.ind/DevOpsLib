import zipfile
import os


def insert_dir_to_zip(zip_file, folder_to_add):
    """
    Insert a dir whit files into a .zip
    :param zip_file: Str: name of the .zip
    :param folder_to_add: Str name of the folder to add
    :return:
    """
    # Verifico si el directorio a agregar existe
    if not os.path.exists(folder_to_add):
        print(f"El directorio '{folder_to_add}' no existe.")
        return
    # Creo el objeto zip
    with zipfile.ZipFile(zip_file, 'a') as zipobj:
        for foldername, subfolders, filenames in os.walk(folder_to_add):
            for filename in filenames:
                filepath = str(os.path.join(foldername, filename))
                zipobj.write(filepath)
    print(f"Dir '{folder_to_add}' add to '{zip_file}'.")


def insert_file_to_zip(zip_file, file_to_add):
    """
    Insert a file into a .zip
    :param zip_file: Str: name of the .zip
    :param file_to_add: Str: name of the file to add
    :return: Message
    """
    # Verifico si el archivo a agregar existe
    if not os.path.exists(file_to_add):
        print(f"El archivo '{file_to_add}' no existe.")
        return
    # Creo el objeto zip
    with zipfile.ZipFile(zip_file, 'a') as zipobj:
        # Para agregar un archivo
        zipobj.write(file_to_add, arcname=file_to_add)
    print(f"File '{file_to_add}' add to '{zip_file}'.")
